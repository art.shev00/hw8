// DOM це програмне середовище для читання та редагування веб документів. ми можемо змінювати структуру, стиль, видаляти і добавляти елементи
// innerHTML повертає все разом з тегами, а innerText повертає лише текст
// document.querySelector() це універсальний спосіб звернення до елементів



let paragraphs = document.querySelectorAll('p');
paragraphs.forEach(element => {
    element.style.backgroundColor = '#ff0000'
});



let id = document.getElementById("optionsList");
console.log(id);
console.log(id.parentElement);
let child = id.childNodes;

child.forEach(el => {
    console.log(el)
})

let a = document.querySelector('#testParagraph');
a.innerHTML = 'This is a paragraph';


let elemMainHeader = document.querySelector('.main-header');

let b  = elemMainHeader.children

for (const el of b) {
    el.className = 'nav-item'
}
console.log(b);


let sectionTitle = document.querySelectorAll('.section-title');
console.log(sectionTitle);

for (const element of sectionTitle) {
    element.classList.remove('section-title');
}
console.log(sectionTitle);